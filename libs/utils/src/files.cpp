#include <sys/sendfile.h>
#include <sys/ioctl.h>
#include <linux/fs.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <libgen.h>

#include <utils/utils.hpp>

using namespace std;

ssize_t fd_path(int fd, char *path, size_t size) {
	snprintf(path, size, "/proc/self/fd/%d", fd);
	return xreadlink(path, path, size);
}

int fd_pathat(int dirfd, const char *name, char *path, size_t size) {
	if (fd_path(dirfd, path, size) < 0)
		return -1;
	auto len = strlen(path);
	path[len] = '/';
	strncpy(path + len + 1, name, size - len - 1);
	return 0;
}

int mkdirs(string path, mode_t mode) {
	errno = 0;
	for (char *p = path.data() + 1; *p; ++p) {
		if (*p == '/') {
			*p = '\0';
			if (mkdir(path.data(), mode) == -1) {
				if (errno != EEXIST)
					return -1;
			}
			*p = '/';
		}
	}
	if (mkdir(path.data(), mode) == -1) {
		if (errno != EEXIST)
			return -1;
	}
	return 0;
}

static void post_order_walk(int dirfd, const function<void(int, dirent *)> &&fn) {
	auto dir = xopen_dir(dirfd);
	if (!dir) return;

	for (dirent *entry; (entry = xreaddir(dir.get()));) {
		if (entry->d_type == DT_DIR)
			post_order_walk(xopenat(dirfd, entry->d_name, O_RDONLY | O_CLOEXEC), std::move(fn));
		fn(dirfd, entry);
	}
}

static void pre_order_walk(int dirfd, const function<bool(int, dirent *)> &&fn) {
	auto dir = xopen_dir(dirfd);
	if (!dir) return;

	for (dirent *entry; (entry = xreaddir(dir.get()));) {
		if (!fn(dirfd, entry))
			continue;
		if (entry->d_type == DT_DIR)
			pre_order_walk(xopenat(dirfd, entry->d_name, O_RDONLY | O_CLOEXEC), std::move(fn));
	}
}

static void remove_at(int dirfd, struct dirent *entry) {
	unlinkat(dirfd, entry->d_name, entry->d_type == DT_DIR ? AT_REMOVEDIR : 0);
}

void rm_rf(const char *path) {
	struct stat st;
	if (lstat(path, &st) < 0)
		return;
	if (S_ISDIR(st.st_mode))
		frm_rf(xopen(path, O_RDONLY | O_CLOEXEC));
	remove(path);
}

void frm_rf(int dirfd) {
	post_order_walk(dirfd, remove_at);
}


void mv_dir(int src, int dest) {
	auto dir = xopen_dir(src);
	run_finally f([=]{ close(dest); });
	for (dirent *entry; (entry = xreaddir(dir.get()));) {
		switch (entry->d_type) {
		case DT_DIR:
			if (faccessat(dest, entry->d_name, F_OK, 0) == 0) {
				// Destination folder exists, needs recursive move
				int newsrc = xopenat(src, entry->d_name, O_RDONLY | O_CLOEXEC);
				int newdest = xopenat(dest, entry->d_name, O_RDONLY | O_CLOEXEC);
				mv_dir(newsrc, newdest);
				unlinkat(src, entry->d_name, AT_REMOVEDIR);
				break;
			}
			// Else fall through
		case DT_LNK:
		case DT_REG:
			renameat(src, entry->d_name, dest, entry->d_name);
			break;
		}
	}
}


void *__mmap(const char *filename, size_t *size, bool rw) {
	struct stat st;
	void *buf;
	int fd = xopen(filename, (rw ? O_RDWR : O_RDONLY) | O_CLOEXEC);
	fstat(fd, &st);
	if (S_ISBLK(st.st_mode))
		ioctl(fd, BLKGETSIZE64, size);
	else
		*size = st.st_size;
	buf = *size > 0 ? xmmap(nullptr, *size, PROT_READ | (rw ? PROT_WRITE : 0), MAP_SHARED, fd, 0) : nullptr;
	close(fd);
	return buf;
}

void fd_full_read(int fd, void **buf, size_t *size) {
	*size = lseek(fd, 0, SEEK_END);
	lseek(fd, 0, SEEK_SET);
	*buf = xmalloc(*size + 1);
	xxread(fd, *buf, *size);
	((char *) *buf)[*size] = '\0';
}

void full_read(const char *filename, void **buf, size_t *size) {
	int fd = xopen(filename, O_RDONLY | O_CLOEXEC);
	if (fd < 0) {
		*buf = nullptr;
		*size = 0;
		return;
	}
	fd_full_read(fd, buf, size);
	close(fd);
}

string fd_full_read(int fd) {
	char buf[4096];
	string str;
	for (ssize_t len; (len = xread(fd, buf, sizeof(buf))) > 0;)
		str.insert(str.end(), buf, buf + len);
	return str;
}

string full_read(const char *filename) {
	int fd = xopen(filename, O_RDONLY | O_CLOEXEC);
	run_finally f([=]{ close(fd); });
	return fd < 0 ? "" : fd_full_read(fd);
}

void write_zero(int fd, size_t size) {
	char buf[4096] = {0};
	size_t len;
	while (size > 0) {
		len = sizeof(buf) > size ? size : sizeof(buf);
		write(fd, buf, len);
		size -= len;
	}
}

void file_readline(bool trim, const char *file, const function<bool(string_view)> &fn) {
	FILE *fp = xfopen(file, "re");
	if (fp == nullptr)
		return;
	size_t len = 1024;
	char *buf = (char *) malloc(len);
	char *start;
	ssize_t read;
	while ((read = getline(&buf, &len, fp)) >= 0) {
		start = buf;
		if (trim) {
			while (read && (buf[read - 1] == '\n' || buf[read - 1] == ' '))
				--read;
			buf[read] = '\0';
			while (*start == ' ')
				++start;
		}
		if (!fn(start))
			break;
	}
	fclose(fp);
	free(buf);
}

void parse_prop_file(const char *file, const function<bool(string_view, string_view)> &&fn) {
	file_readline(true, file, [&](string_view line_view) -> bool {
		char *line = (char *) line_view.data();
		if (line[0] == '#')
			return true;
		char *eql = strchr(line, '=');
		if (eql == nullptr || eql == line)
			return true;
		*eql = '\0';
		return fn(line, eql + 1);
	});
}

void parse_mnt(const char *file, const function<bool(mntent*)> &fn) {
	auto fp = sFILE(setmntent(file, "re"), endmntent);
	if (fp) {
		mntent mentry{};
		char buf[4096];
		while (getmntent_r(fp.get(), &mentry, buf, sizeof(buf))) {
			if (!fn(&mentry))
				break;
		}
	}
}
