cmake_minimum_required(VERSION 3.0.0)
project(libutils VERSION 0.1.0)

set(CMAKE_BUILD_TYPE Release)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Oz -fomit-frame-pointer -flto")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17")

include_directories(include)

file(GLOB SOURCES "src/*.cpp")

add_library(utils STATIC ${SOURCES})