#include <sys/mount.h>
#include <sys/un.h>
#include <unistd.h>
#include <stdlib.h>
#include <vector>

#include <utils/logging.hpp>

struct cmdline {
	bool skip_initramfs;
	bool force_normal_boot;
	char slot[3];
	char dt_dir[64];
	char hardware[32];
	char hardware_plat[32];
};

struct raw_data {
	uint8_t *buf = nullptr;
	size_t sz = 0;

	raw_data() = default;
	raw_data(const raw_data&) = delete;
	raw_data(raw_data &&d) {
		buf = d.buf;
		sz = d.sz;
		d.buf = nullptr;
		d.sz = 0;
	}
	~raw_data() {
		free(buf);
	}
};

struct fstab_entry {
	std::string dev;
	std::string mnt_point;
	std::string type;
	std::string mnt_flags;
	std::string fsmgr_flags;

	fstab_entry() = default;
	fstab_entry(const fstab_entry &o) = delete;
	fstab_entry(fstab_entry &&o) = default;
	void to_file(FILE *fp);
};

#define DEFAULT_DT_DIR "/proc/device-tree/firmware/android"

void load_kernel_info(cmdline *cmd);

class BaseInit {
protected:
	cmdline *cmd;
	char **argv;
	std::vector<std::string> mount_list;

	void exec_init() {
		cleanup();
		execv("/init", argv);
		exit(1);
	}
	virtual void early_mount() = 0;
	virtual void cleanup();
public:
	BaseInit(char *argv[], cmdline *cmd) :
	cmd(cmd), argv(argv), mount_list{"/sys", "/proc"} {}
	virtual ~BaseInit() = default;
	virtual void start() = 0;
	void read_dt_fstab(std::vector<fstab_entry> &fstab);
	void dt_early_mount();
};

class SARInit : public BaseInit {
protected:
	raw_data self;
	raw_data config;
	std::vector<raw_file> overlays;
	std::string persist_dir;

	void early_mount() override;
	void mount_system_root();
public:
	SARInit(char *argv[], cmdline *cmd)
	 : BaseInit(argv, cmd) {}
		void start() override {
			early_mount();
			exec_init();
	 }
	
};
