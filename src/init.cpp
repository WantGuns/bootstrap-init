#include <sys/stat.h>

#include <utils/utils.hpp>

#include <init/init.hpp>

using namespace std;

int main(int argc, char *argv[]) {
	umask(0);

	if (getpid() != 1)
		return 1;
	
	BaseInit *init;
	cmdline cmd{};

	load_kernel_info(&cmd);
	
	init = new SARInit(argv, &cmd);
	init->start();
	exit(1);
}
