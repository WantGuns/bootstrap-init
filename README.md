# Bootstrap-Init

This project is the source for the `init` executed by the LXC container to bootstrap Android's `init`.

## Build

> GCC based builds do not work. An LLVM toolchain is necessary for building

```bash
$ git clone https://gitlab.com/WantGuns/bootstrap-init
$ cd bootstrap-init/build
$ cmake \
    -DCMAKE_C_COMPILER=$(which clang) \
    -DCMAKE_CXX_COMPILER=$(which clang++) \
    ..
$ make
```

## Credits

The source was originally taken from [Magisk](https://github.com/topjohnwu/Magisk).
